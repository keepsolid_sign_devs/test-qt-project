#ifndef FILMSWINDOW_H
#define FILMSWINDOW_H

#include <QAbstractItemModel>
#include <QMainWindow>

#include "filmlistmodel.h"

namespace Ui {
class FilmsWindow;
}

class FilmsWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit FilmsWindow(QWidget *parent = 0);
    ~FilmsWindow();

private slots:
    void onClosed();
    void onAddFilm();
    void onDeleteFilm(const int& index);

private:
    FilmListModel filmsModel;
    void createFormInterior();

};

#endif // FILMSWINDOW_H
