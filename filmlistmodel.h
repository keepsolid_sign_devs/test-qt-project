#ifndef FILMLISTMODEL_H
#define FILMLISTMODEL_H

#include <QAbstractListModel>
#include <QList>
#include <memory>

#include "film.h"

class FilmListModel : public QAbstractListModel
{
    Q_OBJECT

public:
    enum FilmRoles {
        NameRole = Qt::UserRole,
        PreviewRole = Qt::UserRole + 1,
        DescriptionRole = Qt::UserRole + 2,
        YearRole = Qt::UserRole + 3
    };

    FilmListModel(QObject *parent = 0);
    ~FilmListModel();

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    Qt::ItemFlags flags(const QModelIndex &index) const;

    Q_INVOKABLE Film* itemAt(int index) const {
        return filmList.at(index);
    }

    void addFilm(const QString &name, const QString &description, const int &year);
    void addFilm(const QString &name, const QString &previewSource, const QString &description, const int &year);
    bool removeFilmAt(const int &index);

protected:
    QHash<int, QByteArray> roleNames() const;

private:
    QList<Film*> filmList;

    friend Film::~Film();
};

#endif // FILMLISTMODEL_H
