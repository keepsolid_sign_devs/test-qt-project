#include <QDebug>
#include "film.h"

Film::Film(QObject *parent)
    : QObject(parent)
{
}

Film::Film(const QString &name, const QString &previewSource, const QString &description, const int& year, QObject *parent)
    : QObject(parent), m_name(name), m_preview_source(previewSource), m_description(description), m_year(year)
{
}

Film::~Film()
{
    qDebug("destructor was called");
}

QString Film::name() const
{
    return m_name;
}

void Film::setName(const QString &name)
{
    if (name != m_name) {
        m_name = name;
        emit nameChanged();
    }
}

QString Film::previewSource() const
{
    return m_preview_source;
}

void Film::setPreviewSource(const QString &previewSource)
{
    if (previewSource != m_preview_source) {
        m_preview_source = previewSource;
        emit previewSourceChanged();
    }
}

QString Film::description() const
{
    return m_description;
}

void Film::setDescription(const QString &description)
{
    if (description != m_description) {
        m_description = description;
        emit descriptionChanged();
    }
}

int Film::year() const
{
    return m_year;
}

void Film::setYear(const int &year)
{
    if (year != m_year) {
        m_year = m_year;
        emit yearChanged();
    }
}
