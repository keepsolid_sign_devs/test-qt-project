#include <QHash>

#include "filmlistmodel.h"

FilmListModel::FilmListModel(QObject *parent)
    : QAbstractListModel(parent)
{
}

FilmListModel::~FilmListModel()
{
    for(int i = 0; i < filmList.count(); i++) {
        delete filmList.at(i);
    }
}

QHash<int, QByteArray> FilmListModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[NameRole] = "name";
    roles[PreviewRole] = "previewSource";
    roles[YearRole] = "year";
    return roles;
}

int FilmListModel::rowCount(const QModelIndex &parent) const
{
    return filmList.count();
}

QVariant FilmListModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (index.row() < 0 || index.row() > filmList.count())
        return QVariant();

    const Film *film = itemAt(index.row());
    if (role == NameRole)
        return film->name();
    if (role == PreviewRole)
        return film->previewSource();
    else if (role == YearRole)
        return  QVariant::fromValue(film->year());

    return QVariant();
}

Qt::ItemFlags FilmListModel::flags(const QModelIndex &index) const
{
    Qt::ItemFlags flags = QAbstractItemModel::flags(index);
    if (index.row() != index.column())
        flags |= Qt::ItemIsEditable;
    return flags;
}

void FilmListModel::addFilm(const QString &name, const QString &previewSource, const QString &description, const int &year)
{
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    filmList.append(new Film(name, previewSource, description, year));
    endInsertRows();
}

void FilmListModel::addFilm(const QString &name, const QString &description, const int &year)
{
    addFilm(name, name, description, year);
}

bool FilmListModel::removeFilmAt(const int& index) {
    if (index < 0 || index >= rowCount()) return false;
    beginRemoveRows(QModelIndex(), index, index);
    Film* film = filmList.at(index);
    filmList.removeAt(index);
    endRemoveRows();

    delete film;
    return true;
}


