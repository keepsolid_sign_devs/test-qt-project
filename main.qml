import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3
import QtQuick.Window 2.2
import com.films 1.0

Page {
    anchors.fill: parent
    Layout.minimumWidth: 700

    background: Rectangle {
        anchors.fill: parent
        color: "white"
    }

    header: ToolBar {
        id: toolBar
        width: parent.width
        height: 32
        background: Rectangle{
            anchors.fill: parent
            color: "#2980cc"
        }
        Label {
            width: parent.width
            color: "white"
            padding: 10
            text: qsTr("Short films by David Lynch")
            anchors.verticalCenter: parent.verticalCenter
            font.pointSize: 12
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        MouseArea {
            id: closeBtn
            objectName: "closeBtn"
            signal closed()
            width: 20
            height: 20
            anchors { right: parent.right; rightMargin: 15; verticalCenter: parent.verticalCenter }
            Image {
                id: closeImg
                width: parent.width
                height: parent.height
                sourceSize.width: width
                sourceSize.height: height
                antialiasing: true
                fillMode: Image.PreserveAspectFit
                source: "http://www.5pointwellness.com/images/closebutton.png"
            }
            // Respond to the signal here.
            onClicked: { closed() }
        }
    }

    RowLayout {
        spacing: 0
        anchors { topMargin: 6; top: parent.top; bottom: addBtn.top }
        width: parent.width

        Label {
            text: "Empty list. You can add films manually"
            padding: 10
            visible: listView.count > 0? false : true
            Layout.fillHeight: true
            Layout.preferredWidth: parent.width * 0.3
            Layout.minimumWidth: 250
            horizontalAlignment: Text.AlignHCenter
            elide: Text.ElideRight
            wrapMode: Text.WordWrap
            textFormat: Text.PlainText
            clip: true
            color: "#444444"
            font.pointSize: 14
            font.family: "OpenSans"
        }

        ListView {
            id: listView
            clip: true
            focus: true
            interactive: true
            visible: count > 0? true : false
            Component.onCompleted:{
                listView.forceActiveFocus()
            }
            snapMode: ListView.SnapOneItem
            Layout.fillHeight: true
            Layout.preferredWidth: parent.width * 0.3
            Layout.minimumWidth: 250
            spacing: 0
            currentIndex: 0
            model: filmModel
            delegate: filmDelegate
            highlight: highlight
            highlightFollowsCurrentItem: true
            highlightMoveVelocity: 600
            onCurrentIndexChanged: {
                console.log(listView.currentIndex);
                if (listView.currentIndex < 0) {
                    currFilmImage.source = "films/dummy_film_icon"
                    currFilmName.text = "No name"
                    currFilmDescription.text = "No description"
                    return
                }
                var currItem = model.itemAt(listView.currentIndex);
                // @disable-check M126
                currFilmImage.source = "films/" + (currItem.previewSource.length == 0? "dummy_film_icon" : currItem.previewSource)
                currFilmName.text = currItem.name
                currFilmDescription.text = currItem.description
            }
            ScrollBar.vertical: ScrollBar {
                id:scrollBar
                parent: listView.parent
                policy: ScrollBar.AlwaysOn
                width: 10;
                size: 0.3
                contentItem: Rectangle {
                    implicitWidth: 6
                    implicitHeight: 200
                    color: scrollBar.pressed ? "#2678c0" : "#2980cc"
                }
                background: Rectangle {
                    color: "#ebebeb"
                }
                anchors.top: listView.top
                anchors.left: listView.right
                anchors.bottom: listView.bottom
            }
        }

        Item {
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.bottomMargin: 20
            Layout.leftMargin: 20

            Rectangle {
                anchors { fill: parent }
                color: "#f8f8f8"
            }

            Column {
                anchors { fill: parent }
                spacing: 0
                Layout.minimumWidth: currFilmImage.width

                Item {
                    width: 1
                    height: 15
                }

                Image {
                    id:currFilmImage
                    width: parent.width * 0.5 < 250? 250 : parent.width * 0.5
                    height:  parent.height * 0.3 < 200? 200 : parent.height * 0.3
                    fillMode: Image.PreserveAspectFit
                    anchors { horizontalCenter: parent.horizontalCenter }
                }

                Item {
                    width: 1
                    height: 10
                }

                Text {
                    id: currFilmName
                    width: parent.width
                    horizontalAlignment: Text.AlignHCenter
                    maximumLineCount: 3
                    elide: Text.ElideRight
                    wrapMode: Text.WordWrap
                    textFormat: Text.PlainText
                    clip: true
                    text: "Stewe Webb"
                    color: "#444444"
                    font.pointSize: 24
                    font.family: "OpenSans"
                }

                Item {
                    width: 1
                    height: 10
                }

                RowLayout {
                    width: parent.width
                    spacing: 12
                    height: dvdText
                    Layout.minimumWidth: implicitWidth

                    Label {
                        id: dvdLabel
                        Layout.alignment: Qt.AlignRight
                        Layout.fillWidth: true
                        Layout.minimumWidth: implicitWidth
                        Layout.leftMargin: 10
                        horizontalAlignment: Qt.AlignRight
                        text: "DVD availability:"
                        font.pointSize: 14
                        color: "#5e5e5e"
                        clip:true
                        font.family: "OpenSans"
                    }
                    Text {
                        id: dvdText
                        Layout.alignment: Qt.AlignLeft
                        Layout.fillWidth: true
                        Layout.rightMargin: 10
                        anchors { top: dvdLabel.top }
                        maximumLineCount: 3
                        elide: Text.ElideRight
                        wrapMode: Text.WordWrap
                        textFormat: Text.PlainText
                        clip: true
                        text: "The Short Films of David Lynch & The Lime Green Set"
                        color: "#2a2a2a"
                        font.pointSize: 14
                        font.family: "OpenSans"
                    }
                }

                Item {
                    width: 1
                    height: 20
                }

                Text {
                    id: currFilmDescription
                    width: parent.width
                    padding: 20
                    horizontalAlignment: Qt.AlignHCenter
                    Layout.alignment: Qt.AlignHCenter
                    //                    Layout.fillHeight: true
                    maximumLineCount: 3
                    elide: Text.ElideRight
                    wrapMode: Text.WordWrap
                    textFormat: Text.PlainText
                    clip: true
                    text: "No description"
                    color: "#2a2a2a"
                    font.pointSize: 14
                    font.family: "OpenSans"
                }
            }
            Button {
                id: deleteBtn
                objectName: "deleteBtn"
                flat: true
                text: "Delete"
                anchors { right: parent.right; bottom: parent.bottom; margins: 10 }
                signal deleteFilm(int index)
                background: Rectangle {
                    implicitWidth: 101
                    implicitHeight: 36
                    color: deleteBtn.down ? "#dbdbdb" : "#ebebeb"
                }
                onClicked: deleteFilm(listView.currentIndex)
            }
        }
    }

    Item {
        id: addBtn
        height: addBtnContent.height + divider.height
        width: parent.width
        anchors.bottom: parent.bottom

        Rectangle {
            id: divider
            width: parent.width
            height: 1
            color: "#dbdbdb"
        }

        Rectangle {
            width: parent.width
            height: 30
            anchors.top: divider.bottom
            color: "white"
        }

        Item {
            id: addBtnContent
            width: addBtnLabel.width+addBtnImage.width
            height: 30
            anchors { verticalCenter: parent.verticalCenter; horizontalCenter: parent.horizontalCenter }

            Image {
                id: addBtnImage
                width: 14
                height: 14
                sourceSize.width: 14
                sourceSize.height: 14
                antialiasing: true
                fillMode: Image.PreserveAspectFit
                anchors { verticalCenter: parent.verticalCenter }
                source: "http://www.iconsdb.com/icons/download/tropical-blue/plus-4-128.png"
            }

            Label {
                id: addBtnLabel
                width: Layout.implicitWidth
                height: parent.height
                color: "#444444"
                padding: 10
                text: qsTr("Add film")
                font.pointSize: 12
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                anchors { left: addBtnImage.right }
            }

            MouseArea {
                signal addFilm()
                onAddFilm: console.log("add film clicked")
                objectName: "addBtnListener"
                anchors.fill: parent
                onClicked: addFilm()
            }
        }
    }

    Component {
        id: highlight
        Rectangle {
            width: 180; height: 40
            color: "#2980cc"; /*radius: 5*/
            y: listView.currentItem.y
            Behavior on y {
                SpringAnimation {
                    spring: 2
                    damping: 0.2
                }
            }
        }
    }

    Component {
        id: listHighlight
        Rectangle{
            color: Qt.rgba(41/255, 128/255, 204/255, 0.7)
        }
    }

    Component {
        id: filmDelegate
        ItemDelegate {
            id: wrapper
            width: listView.width - listView.leftMargin - listView.rightMargin
            height: 50
            states:  [
                State {
                    name: "Default"
                    when: !wrapper.ListView.isCurrentItem
                    PropertyChanges{ target: wrapperText; color:"black"}
                },
                State {
                    name: "Current"
                    when: wrapper.ListView.isCurrentItem
                    PropertyChanges{ target: wrapperText; color:"white"}
                }
            ]
            //                transitions: [
            //                    Transition {
            //                        from: "Default"
            //                        to: "Current"
            //                        ColorAnimation { target: wrapperText; duration: 300}
            //                    },
            //                    Transition {
            //                        from: "Current"
            //                        to: "Default"
            //                        ColorAnimation { target: wrapperText; duration: 300}
            //                    }
            //                ]

            Text {
                id: wrapperText
                anchors { verticalCenter: parent.verticalCenter; fill: parent }
                text: model.year + " - " + model.name
                leftPadding: wrapperIcon.width + 20
                rightPadding: 10
                maximumLineCount: 1
                elide: Text.ElideRight
                wrapMode: Text.WrapAnywhere
                textFormat: Text.PlainText
                verticalAlignment: Text.AlignVCenter
                clip: true
                font.pointSize: 12
                font.family: "OpenSans"
                Behavior on color {
                    ColorAnimation { target: wrapperText; duration: 300 }
                }
                Rectangle {
                    id: wrapperIcon
                    width: 34
                    height: 34
                    color: Qt.rgba(Math.random(), Math.random(), Math.random(), 1);
                    anchors { verticalCenter: parent.verticalCenter; left: parent.left; leftMargin: 10 }
                    Image {
                        width: 20
                        height: 20
                        sourceSize.width: 20
                        sourceSize.height: 20
                        antialiasing: true
                        fillMode: Image.PreserveAspectFit
                        anchors { centerIn: parent }
                        source: "films/dummy_film_icon"
                    }
                }
            }
            MouseArea {
                anchors.fill: parent
                onClicked: { listView.currentIndex = index; }
            }
        }
    }
}

