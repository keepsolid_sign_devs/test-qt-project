#ifndef DATAOBJECT_H
#define DATAOBJECT_H

#include <QObject>

class Film : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(QString previewSource READ previewSource WRITE setPreviewSource NOTIFY previewSourceChanged)
    Q_PROPERTY(QString description READ description WRITE setDescription NOTIFY descriptionChanged)
    Q_PROPERTY(int year READ year WRITE setYear NOTIFY yearChanged)

    friend class FilmListModel;

public:
    Film(QObject *parent=0);
    Film(const QString &name, const QString &previewSource, const QString &description, const int &year, QObject *parent=0);
    ~Film();

    QString name() const;
    QString previewSource() const;
    QString description() const;
    int year() const;

    void setYear(const int &year);
    void setPreviewSource(const QString &previewSource);
    void setDescription(const QString &description);
    void setName(const QString &name);

signals:
    void nameChanged();
    void previewSourceChanged();
    void descriptionChanged();
    void yearChanged();

private:
    QString m_name;
    QString m_preview_source;
    QString m_description;
    int m_year;

};

#endif // DATAOBJECT_H
