#include <QApplication>
#include <QQuickView>
#include <QQuickItem>
#include <QStringList>
#include <qqmlcontext.h>
#include <qqml.h>
#include <QLayout>
#include <QStyle>
#include <QDesktopWidget>

#include "filmswindow.h"
#include "film.h"

FilmsWindow::FilmsWindow(QWidget *parent) :
    QMainWindow(parent)
{
    setWindowTitle(tr("Let's do it!"));
    createFormInterior();
}

FilmsWindow::~FilmsWindow()
{
}

void FilmsWindow::createFormInterior()
{
    QQuickView *view = new QQuickView();
    QWidget *container;
    qmlRegisterType<Film>("com.films", 1, 0, "Film");

    container = QWidget::createWindowContainer(view, this);
    view->setSource(QUrl("qrc:/main.qml"));
    setCentralWidget(container);
    setMinimumHeight(600);
    setMinimumWidth(700);
    //    setSizePolicy (QSizePolicy::Preferred, QSizePolicy::Preferred);
    //resize(sizeHint());

    filmsModel.addFilm("Six Men Getting Sick",
                       "Originally untitled, \"Six Men Getting Sick\" is a one-minute color animated film that consists of six loops shown on a sculptured screen of three human-shaped figures (based on casts of Lynch's own head as done by Jack Fisk) that intentionally distorted the film. Lynch's animation depicted six people getting sick: their stomachs grew and their heads would catch fire.",
                       1966);
    filmsModel.addFilm("Absurd Encounter with Fear",
                       "No description",
                       1967);
    filmsModel.addFilm("Fictitious Anacin Commercial",
                       "No description",
                       1967);
    filmsModel.addFilm("The Alphabet",
                       "\"The Alphabet\" combines animation and live action and goes for four minutes. It has a simple narrative structure relating a symbolically rendered expression of a fear of learning.",
                       1968);
    filmsModel.addFilm("The Grandmother",
                       "The short film combines live action and animation. The story revolves around a boy who grows a grandmother to escape neglect and abuse from his parents. It is silent (no dialogue), with soundtrack cues used to convey story.",
                       1970);
    filmsModel.addFilm("The Amputee",
                       "One shot scene with Catherine Coulson about a woman attempting to write a letter while a female nurse (played by Lynch) tends to her leg stumps. It exists in two versions: one that goes for 4 minutes, and one that goes for 5.",
                       1974);
    filmsModel.addFilm("The Cowboy and the Frenchman",
                       "No description",
                       1988);

    QQmlContext *ctxt = view->rootContext();
    ctxt->setContextProperty("filmModel", &filmsModel);



    setGeometry(
                QStyle::alignedRect(
                    Qt::LeftToRight,
                    Qt::AlignCenter,
                    size(),
                    qApp->desktop()->availableGeometry()
                    )
                );

    QObject *addFilmBtn = view->rootObject()->findChild<QObject*>(QString("addBtnListener"));
    QObject *closeBtn = view->rootObject()->findChild<QObject*>(QString("closeBtn"));
    QObject *deleteBtn = view->rootObject()->findChild<QObject*>(QString("deleteBtn"));

    QObject::connect(closeBtn, SIGNAL(closed()), SLOT(onClosed()));
    QObject::connect(addFilmBtn, SIGNAL(addFilm()), SLOT(onAddFilm()));
    QObject::connect(deleteBtn, SIGNAL(deleteFilm(int)), SLOT(onDeleteFilm(const int&)));
}

void FilmsWindow::onClosed() {
    qDebug( "onClosed");
    QCoreApplication::quit();
}

void FilmsWindow::onAddFilm() {
    qDebug("onAddFilm");
    filmsModel.addFilm("XXXX", "", "No description", 9999);
}

void FilmsWindow::onDeleteFilm(const int& index) {
    qDebug("onDeleteFilm");
    filmsModel.removeFilmAt(index);
}
