#include <QApplication>

#include "film.h"
#include "filmswindow.h"

static void closeApp();

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    FilmsWindow window;
    window.show();

    return app.exec();
}
